﻿using UnityEngine;

public class SpiderEvolutionController : MonoBehaviour
{
    [SerializeField, Range(0, 1)]
    private float _evolutionFactor = default;
    [SerializeField] private EvolutionLimb[] _limbs = default;
    [SerializeField] private int _maxTiers = 4;
    [SerializeField] private int _tier;
    public float EvolutionFactor { get => _evolutionFactor; set => _evolutionFactor = value; }
    public float FactorPerTier => 1f / (float)_maxTiers;

    private void Update()
    {
        CheckTier();
        for (int i = 0; i < _limbs.Length; i++)
            _limbs[i].LevelFactorCallback(_evolutionFactor);
    }

    private void CheckTier()
    {
        if (EvolutionFactor >= (_tier + 1) * FactorPerTier)
            OnTierUp();
        else if (EvolutionFactor < (_tier) * FactorPerTier)
            OnTierDown();
    }

    private void OnTierUp()
    {
        _tier++;
        for (int i = 0; i < _limbs.Length; i++)
            _limbs[i].TierChangedCallback(_tier);
    }
    private void OnTierDown()
    {
        _tier--;
        for (int i = 0; i < _limbs.Length; i++)
            _limbs[i].TierChangedCallback(_tier);
    }
}