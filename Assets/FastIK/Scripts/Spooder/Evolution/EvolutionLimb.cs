﻿using UnityEngine;

public abstract class EvolutionLimb : MonoBehaviour
{
    public virtual void LevelFactorCallback(float level) { }
    public virtual void TierChangedCallback(int tier) { }
}