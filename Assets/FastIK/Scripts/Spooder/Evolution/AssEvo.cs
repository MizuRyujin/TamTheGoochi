﻿using UnityEngine;

public class AssEvo : EvolutionLimb
{
    private float _levelFactor;
    private float _activatedFactor;
    [SerializeField] private float _spawnSize = 0.5f;
    [SerializeField] private float _maxSize = 1;
    [SerializeField] private int _spawnAtTier = 1;
    [SerializeField] private GameObject _meshObj = default;

    private void Awake()
    {
        _meshObj.SetActive(false);
    }

    public override void LevelFactorCallback(float level)
    {
        if (!_meshObj.activeSelf) return;
        _levelFactor = level;
        float factor = ((level - _activatedFactor) / (1 - _activatedFactor));
        if (!float.IsInfinity(factor) && !float.IsNaN(factor) && Mathf.Sign(factor) == 1)
            SetSize(_spawnSize + ((_maxSize - _spawnSize) * factor));
    }

    public override void TierChangedCallback(int tier)
    {
        if (tier == _spawnAtTier)
        {
            if (_meshObj.activeSelf)
            {
                _meshObj.SetActive(false);
            }
            else
            {
                SetSize(_spawnSize);
                _activatedFactor = _levelFactor;
                _meshObj.SetActive(true);
            }
        }
    }

    private void SetSize(float size)
    {
        transform.localScale = Vector3.one * size;
    }
}