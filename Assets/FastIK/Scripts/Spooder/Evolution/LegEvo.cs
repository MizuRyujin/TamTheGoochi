﻿using UnityEngine;

public class LegEvo : EvolutionLimb
{
    [SerializeField] private GameObject[] _legs = default;
    private GameObject _lastActivated;
    private void Start()
    {
        foreach (GameObject g in _legs) g.SetActive(false);
        TierChangedCallback(0);
    }
    public override void TierChangedCallback(int tier)
    {
        int index = tier - 1;
        if (tier == 0) return;
        if (index != 0)
            _legs[index - 1].SetActive(false);
        _lastActivated?.SetActive(false);
        _legs[index].SetActive(true);
        _lastActivated = _legs[index];
    }
}