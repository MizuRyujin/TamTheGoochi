﻿using System.Collections;
using UnityEngine;

public class Eyes : MonoBehaviour
{
    [SerializeField] private Material _normal = default;
    [SerializeField] private Material _blink = default;
    [SerializeField] private Material _happy = default;
    [SerializeField] private Material _star = default;

    [Space]

    [SerializeField] private Renderer _lIris = default;
    [SerializeField] private Renderer _rIris = default;

    private Coroutine _activeCor;

    private void Start()
    {
        _activeCor = StartCoroutine(CSpaghettiBlink());
    }

    private void SetMaterial(Material m)
    {
        _lIris.material = m;
        _rIris.material = m;
    }
    private IEnumerator CSpaghettiBlink()
    {
        SetMaterial(_blink);
        yield return new WaitForSeconds(0.1f);
        SetMaterial(_normal);
        yield return new WaitForSeconds(Random.Range(0.4f, 2f));
        if (Random.Range(0f, 1f) <= 0.2f)
            _activeCor = StartCoroutine(CSpaghettiQuickBlink());
        else
            _activeCor = StartCoroutine(CSpaghettiBlink());
    }
    private IEnumerator CSpaghettiQuickBlink()
    {
        SetMaterial(_blink);
        yield return new WaitForSeconds(0.08f);
        SetMaterial(_normal);
        yield return new WaitForSeconds(0.1f);
        SetMaterial(_blink);
        yield return new WaitForSeconds(0.08f);
        SetMaterial(_normal);
        yield return new WaitForSeconds(Random.Range(1f, 2f));
        _activeCor = StartCoroutine(CSpaghettiBlink());
    }
}