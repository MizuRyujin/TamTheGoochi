﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HandTargetIK : MonoBehaviour
{
    private const float NEAR_TARGET_DISTANCE = 0.5f;
    [SerializeField] private Transform _targetGroundChecker = default;
    [SerializeField] private string _sizeReffName = "Spooder";
    [SerializeField] private HandTargetIK _reliesOn = default;
    [SerializeField] private AnimationCurve _heightCurve = default;
    [SerializeField] private float _groundCheckDistance = 10f;
    [SerializeField] private float _stepOffset = 2.3f;
    [SerializeField] private float _stepSpeed = 25f;
    private float StepSpeed => _stepSpeed * SizeFactor * 2f;
    [SerializeField] private float _maxStepDistance = 2f;
    [SerializeField] private bool _startInFront = default;
    [SerializeField] private bool _detachOnAwake = true;
    [SerializeField] private bool _hideInHierarchy = true;
    [Space]
    [SerializeField] private UnityEvent OnGrounded = default;

    private LayerMask _obstaclesLayer;
    private Transform _sizeReference;
    private RaycastHit _previousTargetGroundHit;
    private RaycastHit _currentTargetGroundHit;
    private RaycastHit _groundHit;
    //
    private int _characterMovementDirection;
    private Vector3 _previousPosition;
    //
    private float _timeOfLastMoveEnd;
    private float SizeFactor => _sizeReference.localScale.x;
    private float StepDistance => _maxStepDistance * SizeFactor;
    private float StepOffset => _stepOffset * SizeFactor;
    private Vector3 _curvePositionOffset;

    // Ignores Y
    private float DistanceToGroundHit
    {
        get
        {
            // float distanceToPoint =
            //     Vector3.Distance(_currentTargetGroundHit.point, _groundHit.point);
            float distanceToPoint;
            Vector3 groundPointNoY = _groundHit.point;
            Vector3 currentGroundPointNoY = _currentTargetGroundHit.point;
            groundPointNoY.y = 0;
            currentGroundPointNoY.y = 0;
            distanceToPoint = Vector3.Distance(groundPointNoY, currentGroundPointNoY);
            return distanceToPoint;
        }
    }
    // private bool ShouldUpdateGroundTarget => DistanceToGroundHit >= _maxStepDistance &&
    //     (IsGrounded) || DistanceToGroundHit >= _maxStepDistance * 1.3f;
    private bool ShouldUpdateGroundTarget => (DistanceToGroundHit >= StepDistance &&
        _reliesOn.IsGrounded && IsGrounded) || DistanceToGroundHit >= (StepDistance + StepOffset) * 1.3f;
    public bool IsNearGroundTarget => Vector3.Distance(_currentTargetGroundHit.point, transform.position) <= NEAR_TARGET_DISTANCE;
    public bool IsGrounded { get; private set; }

    private void Awake()
    {
        _obstaclesLayer = LayerMask.GetMask("Obstacle");
        if (_detachOnAwake) transform.SetParent(null);
        if (_hideInHierarchy) transform.hideFlags = HideFlags.HideInHierarchy;
        _sizeReference = GameObject.Find(_sizeReffName).transform;
        if (_sizeReference == null) Debug.LogError("Size reference not found");
        OnGrounded.AddListener(gameObject.AddComponent<Scripts.WalkSFX>().PlaySFX);
    }

    private void Start()
    {
        GetStartingGroundPoint(_startInFront);
        transform.position = _groundHit.point;
    }

    private void FixedUpdate()
    {
        if (!_targetGroundChecker.gameObject.activeInHierarchy) return;
        GetMovementDirection();
        GetGroundPoint();
    }

    private void LateUpdate()
    {
        if (!_targetGroundChecker.gameObject.activeInHierarchy) return;
        MoveTowardsGroundPoint();
    }

    private void GetMovementDirection()
    {
        Vector3 directionVector = _previousPosition - _targetGroundChecker.position;
        _characterMovementDirection = Vector3.Dot(_targetGroundChecker.forward, directionVector * 2) > 0 ? -1 : 1;
        _previousPosition = _targetGroundChecker.position;
    }

    private void GetStartingGroundPoint(bool startInFront)
    {
        RaycastHit hit;
        if (Physics.Raycast(_targetGroundChecker.position, -_targetGroundChecker.up, out hit, _groundCheckDistance, _obstaclesLayer))
        {
            _groundHit = hit;
            Vector3 positionOffset;
            int directionSign;
            directionSign = startInFront ? 1 : 0;
            positionOffset = ((StepDistance * 0.99f) * directionSign * _targetGroundChecker.forward);
            if (Physics.Raycast(_targetGroundChecker.position + positionOffset, -_targetGroundChecker.up, out hit, _groundCheckDistance, _obstaclesLayer))
                NewTargetGroundHit(hit);
            else
                Debug.LogError("Unable to get IK starting position offset!");
        }
        else
            Debug.LogError("Unable to get IK starting positions!");
        _previousTargetGroundHit = _currentTargetGroundHit;
    }

    private void GetGroundPoint()
    {
        RaycastHit hit;
        if (Physics.Raycast(_targetGroundChecker.position, -_targetGroundChecker.up, out hit, _groundCheckDistance, _obstaclesLayer))
        {
            _groundHit = hit;
            if (ShouldUpdateGroundTarget)
            {
                int directionSign;
                directionSign = Vector3.Dot(_targetGroundChecker.forward, transform.InverseTransformPoint(_groundHit.point)) > 0 ? 1 : -1;
                if (directionSign == _characterMovementDirection && !GetTargetGroundHit(_groundHit, directionSign))
                {
                    Debug.Log("aa");
                    NewTargetGroundHit(_groundHit);
                }
            }
        }
    }

    private bool GetTargetGroundHit(RaycastHit fromHit, int directionSign)
    {
        RaycastHit hit;
        Vector3 positionOffset;
        positionOffset = (StepOffset * directionSign * _targetGroundChecker.forward);
        if (Physics.Raycast(_targetGroundChecker.position + positionOffset, -_targetGroundChecker.up, out hit, _groundCheckDistance, _obstaclesLayer))
        {
            NewTargetGroundHit(hit);
            return true;
        }
        return false;
    }

    private void NewTargetGroundHit(RaycastHit hit)
    {
        IsGrounded = false;
        _previousTargetGroundHit = _currentTargetGroundHit;
        _currentTargetGroundHit = hit;
    }

    private void MoveTowardsGroundPoint()
    {
        Vector3 desiredPos = _currentTargetGroundHit.point;
        if (!IsGrounded)
        {
            float remainingDistance = Vector3.Distance(transform.position - _curvePositionOffset, desiredPos);
            float totalDistance = Vector3.Distance(_previousTargetGroundHit.point, desiredPos);
            float traveledDistancePercentage = 1 - (remainingDistance / totalDistance);
            float heightCurveOffset = 0;
            if (!float.IsNaN(traveledDistancePercentage) && !float.IsInfinity(traveledDistancePercentage))
                heightCurveOffset = _heightCurve.Evaluate(traveledDistancePercentage) * SizeFactor;
            _curvePositionOffset = new Vector3(0, heightCurveOffset, 0); // ! does not take into account boy rotations
            if (remainingDistance > 0 && totalDistance > 0)
                desiredPos += _curvePositionOffset;
            //if (!float.IsNaN(desiredPos.point.x))
            transform.position = Vector3.MoveTowards(transform.position, desiredPos, Time.deltaTime * StepSpeed);
            if (remainingDistance <= 0.005f)
            {
                IsGrounded = true;
                _timeOfLastMoveEnd = Time.time;
                transform.position = _currentTargetGroundHit.point;
                OnGrounded?.Invoke();
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (_targetGroundChecker == null) return;

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(_targetGroundChecker.position, Vector3.one * 0.3f);
        Gizmos.DrawRay(_targetGroundChecker.position, -_targetGroundChecker.up * _groundCheckDistance);
        if (Application.isPlaying)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(transform.position, 0.2f);
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(_groundHit.point, 0.2f);
            Gizmos.color = Color.black;
            Gizmos.DrawRay(_groundHit.point, _targetGroundChecker.forward * StepDistance);
            Gizmos.color = Color.white;
            Gizmos.DrawLine(_currentTargetGroundHit.point, _groundHit.point);
        }
    }
}