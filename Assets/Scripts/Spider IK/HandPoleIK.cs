﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandPoleIK : MonoBehaviour
{
    [SerializeField] private Transform _reparentTo = default;
    private void Awake()
    {
        transform.parent = _reparentTo;
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position, 0.1f);
    }
}