﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SingleBoneIK : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private Transform headBone;
    [SerializeField] private float     speed;
    [SerializeField] private float     maxBoneAngle;

    // Bone manipulation
    private void LateUpdate()
    {
        Quaternion localRot = headBone.localRotation;
        headBone.localRotation = Quaternion.identity;

        Vector3 vecToTarget = target.position - headBone.position;
        Vector3 vecToTargetLocal = headBone.InverseTransformDirection(vecToTarget);

        // Limit bone
        vecToTargetLocal = Vector3.RotateTowards(Vector3.forward, vecToTargetLocal,
            Mathf.Deg2Rad * maxBoneAngle, 0);

        Quaternion targetRot = Quaternion.LookRotation(vecToTargetLocal, Vector3.up);
        
        headBone.localRotation = Quaternion.Slerp
            (localRot, targetRot, 1 - Mathf.Exp(-speed * Time.deltaTime));
    }
}
