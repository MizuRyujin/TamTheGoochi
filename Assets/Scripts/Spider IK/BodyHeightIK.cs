﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyHeightIK : MonoBehaviour
{
    [SerializeField] private float _bodyYOffset = 0;
    [SerializeField] private HandTargetIK[] _hands = default;

    private void Awake()
    {

    }

    private void LateUpdate()
    {
        Vector3 newLocalPos = transform.localPosition;
        float y = 0;
        for (int i = 0; i < _hands.Length; i++)
            y += _hands[i].transform.position.y;
        y /= _hands.Length;
        newLocalPos.y = y + _bodyYOffset;
        transform.localPosition = Vector3.MoveTowards(newLocalPos, transform.localPosition, Time.deltaTime * 0.1f);
    }

}