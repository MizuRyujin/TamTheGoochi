﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheckerIK : MonoBehaviour
{
    [SerializeField] private Transform _optionalForwardRedirection = default;
    // Start is called before the first frame update
    void Awake()
    {
        transform.forward = _optionalForwardRedirection.forward;
    }
}
