﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectToggle : MonoBehaviour
{
    [SerializeField] private GameObject _objectOther = default;
    public void Toggle()
    {
        if (_objectOther)
            _objectOther.SetActive(!_objectOther.activeInHierarchy);
        else
            gameObject.SetActive(!gameObject.activeInHierarchy);
    }
}
