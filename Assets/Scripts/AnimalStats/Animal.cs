﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StatType
{
    CLEAN,
    HUNGER,
    HAPPY
}

public class Animal : MonoBehaviour
{
    [SerializeField] EntityStats _stats;

    public EntityStats Stats => _stats;

    public float Dirtyness {get; private set;}
    public float Hunger {get; private set;}
    public float Happiness {get; private set;}

    private void Awake() 
    {
        Dirtyness = _stats.Dirtyness;
        Hunger = _stats.Hunger;
        Happiness = _stats.Happiness;    
    }

    private void Update()
    {
        DecayStats(Time.deltaTime);
    }

    /// <summary>
    /// Decays the 3 main stats
    /// </summary>
    /// <param name="delta"> Time delta time</param>
    public void DecayStats(float delta)
    {
        // Calculate the current amount of the stat from 0 to 1
        float currentAmount = (Dirtyness / _stats.Dirtyness);
        // Calculate the amount of TIME left to decay the stat
        float d = currentAmount * _stats.DirtyDecay;

       // Decay the stat from the current value with the amount of time left 
        Dirtyness = Mathf.Lerp(currentAmount, 0, delta / d) * _stats.Dirtyness;
        _stats.OnDirtynessChange(Dirtyness / _stats.Dirtyness, true);

        currentAmount = (Hunger / _stats.Hunger);
        d = currentAmount * _stats.HungerDecay;
        Hunger = Mathf.Lerp(currentAmount, 0, delta / d) * _stats.Hunger;
        _stats.OnHungerChange(Hunger / _stats.Hunger, true);

        currentAmount = (Happiness / _stats.Happiness);
        d = currentAmount * _stats.HappinessDecay;
        Happiness = Mathf.Lerp(currentAmount, 0, delta / d) * _stats.Happiness;
        _stats.OnHappinessChange(Happiness / _stats.Happiness, true);
    }

    /// <summary>
    /// Method to clean the being
    /// </summary>
    /// <param name="cleanValue">
    /// How much the item that cleans actually cleans
    /// </param>
    public void ChangeStat(float value, StatType type)
    {
        switch (type)
        {
            case StatType.CLEAN:
                Dirtyness += value;
                _stats.OnDirtynessChange(value, false);
                Hunger -= value / 2;
                _stats.OnHungerChange(-value / 2, false);
                break;
            case StatType.HAPPY:
                Happiness += value;
                _stats.OnHappinessChange(value, false);
                break;
            case StatType.HUNGER:
                Hunger += value;
                _stats.OnHungerChange(value, false);
                Dirtyness -= value / 2;
                _stats.OnDirtynessChange(-value, false);
                break;
        }
    }
}