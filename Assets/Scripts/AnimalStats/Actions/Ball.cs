﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : StatModifier
{
    [SerializeField] private Collider _collider;
    private Animal _animal;
    private Rigidbody _rb;

    private void Awake() 
    {
        _rb = GetComponentInParent<Rigidbody>();
        NoiseManager.AddAudioSource(this.gameObject);
        _collider = GetComponent<Collider>();
    }

    public void PickUp(Animal animal, Transform mouth = null)
    {
        _collider.enabled = false;
        if (!mouth)
        {
            transform.parent = animal.transform;
        }
        else
        {
            NoiseManager.PlaySound(this.gameObject, "spider_joy");
            transform.parent = mouth;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }
        
        _animal = animal;
    }

    public void Drop()
    {
        _collider.enabled = true;
        _animal = null;
        transform.parent = null;
        _rb.useGravity = true;
        _rb.isKinematic = false;
    }

    private void Update() 
    {
        if (_animal)
        {
            _animal.ChangeStat(Time.deltaTime * _amount, ModifyingStat);
        }    
    }
}
