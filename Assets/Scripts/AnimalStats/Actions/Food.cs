﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : StatModifier
{
    [SerializeField] private GameObject[] _foodPrefabs;
    [SerializeField] private float _eatSoundTimer;

    private Transform _meshHolderTransform;

    private WaitForSeconds timeToEat;

    private void Awake()
    {
        _meshHolderTransform = transform.Find("Mesh");
        timeToEat = new WaitForSeconds(_eatSoundTimer);
    }

    private void Start()
    {
        GameObject ob = Instantiate(_foodPrefabs[Random.Range(0, _foodPrefabs.Length - 1)],
            _meshHolderTransform.position, _meshHolderTransform.rotation);
        ob.transform.parent = _meshHolderTransform;
    }

    public void Eat(Animal animal)
    {
        StartCoroutine(EatingCoRoutine(animal));
    }

    private IEnumerator EatingCoRoutine(Animal animal)
    {
        NoiseManager.AddAudioSource(this.gameObject);
        NoiseManager.PlaySound(this.gameObject, "spider_eating", false, false, true);
        yield return timeToEat;
        NoiseManager.PlaySound(this.gameObject,"Swallow");
        animal.ChangeStat(_amount, ModifyingStat);
        this.gameObject.SetActive(false);
    }
}