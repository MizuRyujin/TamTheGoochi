using UnityEngine;

public class StatModifier : MonoBehaviour
{
    [Header("Stat modifier")]
    [SerializeField] private StatType _modifyingStat = default;
    [SerializeField] protected float _amount = 1f;

    public StatType ModifyingStat => _modifyingStat;
}