﻿using UnityEngine;

public class Shower : StatModifier
{
    private const float DISTANCE = .5f;
    [Header("Shower")]
    [SerializeField] private float _radius = .4f;
    [SerializeField] private LayerMask _layerMask = default;
    [SerializeField] private Transform _overrideRayTransform;

    private ParticleSystem _showerWaterParticles;
    private ParticleSystem _showerFoamParticles;
    private bool _cleaning = false;

    public bool Cleaning 
    {
        get => _cleaning;
        set 
        {
            _cleaning = value;
            
            // Disable particles and enable here
            if (_cleaning)
            {
                _showerWaterParticles.Play();
                _showerFoamParticles.Play();
            }
            else
            {
                _showerWaterParticles.Stop();
                _showerFoamParticles.Stop();
            }

        }
    }

    private void Start() 
    {
        _showerWaterParticles = transform.Find("WaterParticles").GetComponent<ParticleSystem>();
        _showerFoamParticles = transform.Find("FoamParticles").GetComponent<ParticleSystem>();
        Cleaning = false;
    }

    private void FixedUpdate() 
    {
        RaycastHit hit;
        Transform rayT = _overrideRayTransform ? _overrideRayTransform : transform;
        if (Physics.SphereCast(rayT.position, _radius, rayT.forward,
            out hit, DISTANCE, _layerMask))
        {
            Animal animal;
            if (hit.transform.TryGetComponent<Animal>(out animal))
            {
                Cleaning = true;
                animal.ChangeStat(Time.fixedDeltaTime * _amount, ModifyingStat);
                _showerFoamParticles.transform.position = hit.point;
                _showerFoamParticles.transform.up = hit.normal;
            }
            else
            {
                Cleaning = false;
            }
        }
        else
        {
            Cleaning = false;
        }
    }

    private void OnDrawGizmos() 
    {
        Transform rayT = _overrideRayTransform ? _overrideRayTransform : transform;
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(rayT.position, _radius);
        Gizmos.DrawRay(rayT.position, rayT.forward * (DISTANCE + _radius));
    }
}
