﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;
using System;

/// <summary>
/// Name of the activity, this is equal to the name of the objects in
/// the activity object holder in Controller
/// </summary>
public enum Activity
{
    Shower,
    Ball,
    Food
}

public class Controller : MonoBehaviour
{
    private Transform _activityObjectHolder;
    private int _currentActivity;
    private GameObject[] _activityObjects;
    private Grabbable _grababble;
    [SerializeField] private Collider _collider;

    public event Action<bool> onGrabChange;
    private bool lastFrameHeld;

    private void Awake()
    {
        _grababble = GetComponent<Grabbable>();
        _activityObjectHolder = transform.Find("ActiveObjectHolder");
        _currentActivity = 0;

        _activityObjects = new GameObject[_activityObjectHolder.childCount];

        for (int i = 0; i < _activityObjectHolder.childCount; i++)
        {
            _activityObjects[i] = _activityObjectHolder.GetChild(i).gameObject;
        }
    }

    private void Update() {
        _collider.enabled = !_grababble.BeingHeld;

        if (_grababble.BeingHeld != lastFrameHeld)
        {
            onGrabChange?.Invoke(_grababble.BeingHeld);
        }

        lastFrameHeld = _grababble.BeingHeld;
    }

    public void NextActivity()
    {
        _currentActivity++;
        _currentActivity = _currentActivity % 3;

        Debug.Log(_currentActivity);
        for (int i = 0; i < _activityObjects.Length; i++)
        {
            if (i == _currentActivity)
            {
                _activityObjects[i].SetActive(true);
            }
            else
            {
                _activityObjects[i].GetComponent<PositionSet>().PhysicsReset();
                _activityObjects[i].SetActive(false);
            }
        }
    }

    public void PreviousActivity()
    {
        _currentActivity--;
        _currentActivity = (_currentActivity + 3) % 3;
        for (int i = 0; i < _activityObjects.Length; i++)
        {
            if (i == _currentActivity)
            {
                _activityObjects[i].SetActive(true);
            }
            else
            {
                _activityObjects[i].GetComponent<PositionSet>().PhysicsReset();
                _activityObjects[i].SetActive(false);
            }
        }
    }

    public void DisableActivities()
    {
        for (int i = 0; i < _activityObjects.Length; i++)
        {
            _activityObjects[i].GetComponent<PositionSet>().PhysicsReset();
            _activityObjects[i].SetActive(false);
        }
    }
}
