﻿using UnityEngine;
using System;

[CreateAssetMenu(fileName = "PhobiaTrigger", menuName = "ScriptableObjects/Beings", order = 1)]
public class EntityStats : ScriptableObject
{
    private const float MAX_STAT = 100f;

    //* Class variables
    [Tooltip("Being name. Ex: Spider or Snake")]
    [SerializeField] private string _beingName = default;
    [SerializeField] private float _decayTimeMinutes = 10f; // 2 minutes

    [Header("Base stat values")]
    [SerializeField] private float _dirtyness = default;
    [SerializeField] private float _hunger = default;
    [SerializeField] private float _happiness = default;


    [Header("Values that influence stats changes")]
    [Tooltip("How fast being gets dirty, from 0 to 1")]
    [Range(0f, 1f)]
    [SerializeField] private float _dirtynessDecay = 1f;

    [Tooltip("How fast being gets hungry, from 0 to 1")]
    [Range(0f, 1f)]
    [SerializeField] private float _hungerDecay = 1f;

    [Tooltip("How fast being gets happy, from 0 to 1")]
    [Range(0f, 1f)]
    [SerializeField] private float _happinessDecay = 1f;

    //* Class properties
    private float DECAY_TIME => _decayTimeMinutes * 60f;
    public float Dirtyness => _dirtyness;
    public float Hunger => _hunger;
    public float Happiness => _happiness;
    public string BeingName => _beingName;
    public float DirtyDecay => _dirtynessDecay * DECAY_TIME;
    public float HungerDecay => _hungerDecay * DECAY_TIME;
    public float HappinessDecay => _happinessDecay * DECAY_TIME;

    private void OnValidate() 
    {
        _dirtyness = Mathf.Clamp(_dirtyness, 0, MAX_STAT);    
        _hunger = Mathf.Clamp(_hunger, 0, MAX_STAT);    
        _happiness = Mathf.Clamp(_happiness, 0, MAX_STAT);  
    }

    public void OnDirtynessChange(float amount, bool additive)
    {
        onDirtynessChange?.Invoke(amount, additive);
    }

    public void OnHungerChange(float amount, bool additive)
    {
        onHungerChange?.Invoke(amount, additive);
    }

    public void OnHappinessChange(float amount, bool additive)
    {
        onHappinessChange?.Invoke(amount, additive);
    }

    public event Action<float, bool> onDirtynessChange;
    public event Action<float, bool> onHungerChange;
    public event Action<float, bool> onHappinessChange;
}