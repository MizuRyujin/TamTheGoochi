﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;
using UnityEngine.Events;

[RequireComponent(typeof(Grabbable))]
public class GrabEvent : MonoBehaviour
{
    private Grabbable _grabbable;
    public UnityEvent _onGrab;
    public UnityEvent _onUnGrab;

    private void Awake() {
        _grabbable = GetComponent<Grabbable>();
    }

    private bool _lastFrame;

    // Update is called once per frame
    void Update()
    {
        if(_grabbable.BeingHeld != _lastFrame)
        {
            if (_grabbable.BeingHeld)
            {
                _onGrab?.Invoke();
            }
            else
            {
                _onUnGrab?.Invoke();
            }
        }

        _lastFrame = _grabbable.BeingHeld;
    }
}
