﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Scripts
{
    public class DebugSoundScript : MonoBehaviour
    {
        [SerializeField] private float timer = default;
        private WaitForSeconds timeToFart = default;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        private void Awake()
        {
            timeToFart = new WaitForSeconds(timer);
        }

        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        private void Start()
        {
           // this.gameObject.AddComponent<WalkSFX>().WalkSFXConstructor("spider_step");
            StartCoroutine(PlaySound());
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                NoiseManager.StopSound(this.gameObject);
            }
        }

        private IEnumerator PlaySound(string name)
        {
            NoiseManager.PlaySound(this.gameObject, name);
            yield return timeToFart;
            StartCoroutine(PlaySound(name));
        }

        private IEnumerator PlaySound()
        {
            BlaBla.Invoke();
            yield return timeToFart;
            StartCoroutine(PlaySound());
        }

        public UnityEvent BlaBla;
    }
}

