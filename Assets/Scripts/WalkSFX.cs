﻿using UnityEngine;


namespace Scripts
{
    /// <summary>
    /// Class to be used to play walking sound effects on game objects
    /// </summary>
    public class WalkSFX : MonoBehaviour
    {
        private string _audioName;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        private void Awake()
        {
            _audioName = "spider_step2";
        }

        public void PlaySFX()
        {
            NoiseManager.AddAudioSource(this.gameObject);
            NoiseManager.ChangeAudioSourceParams(this.gameObject, 0.3f, 1f, false);
            NoiseManager.PlaySound(this.gameObject, _audioName, true);
        }
    }
}
