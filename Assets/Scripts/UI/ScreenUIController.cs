﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScreenUIController : MonoBehaviour
{
    private Button[] _buttonsInScreen;
    public GameObject ActivePanel {get; set;}

    private int _currentSelected = 0;
    private bool _firstTime = true;
    private GameObject _titleObject;
    private GameObject _mainMenuPanel;

    private void Start() 
    {
        _titleObject = transform.Find("Title").gameObject;
        _titleObject.SetActive(false);
        _mainMenuPanel = transform.Find("MainMenu").gameObject;
    }

    public void StartUI()
    {
        if (!_firstTime) return;
        _firstTime = true;

        TMPro.TextMeshProUGUI text;
        _titleObject.SetActive(true);
        text = _titleObject.GetComponent<TMPro.TextMeshProUGUI>();

        _titleObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        
        Color c = text.color;
        c.a = 0;
        StartCoroutine(ColorOverTimeText(text.color, 2f, text));
        text.color = c;

        _titleObject.LeanScale(Vector3.one, 2f).setOnComplete(() => 
        {
            StartCoroutine(ColorOverTimeText(c, 2f, text));
            _titleObject.LeanScale(new Vector3(0.5f, 0.5f, 0.5f), 2f).setOnComplete(()=>
            {
                _mainMenuPanel.SetActive(true);
            });
        });
    }

    public void Next(bool dir)
    {
        _currentSelected += dir ? 1 : -1;
        _currentSelected = Mathf.Clamp(_currentSelected, 0, _buttonsInScreen.Length - 1);
        _buttonsInScreen[_currentSelected ].Select();
    }

    IEnumerator ColorOverTimeText(Color endColor, float duration, TMPro.TextMeshProUGUI text)
    {
        float t = 0;

        while(t < 1f)
        {
            t += Time.deltaTime/duration;
            text.color = Color.Lerp(text.color, endColor, t);
            yield return null;
        }
    }
}