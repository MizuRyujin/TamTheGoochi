﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoingUI : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        transform.LeanScale(new Vector3(1.1f, 1.1f, 1.1f), 1f).setEaseInBack().setLoopPingPong();
    }

}
