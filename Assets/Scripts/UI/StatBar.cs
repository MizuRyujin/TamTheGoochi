﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class StatBar : MonoBehaviour
{
    [SerializeField] private StatType _type = default;
    [SerializeField] private EntityStats _stats = default;
    [SerializeField] private Color _fillColor = default;
    [SerializeField, Range(0f,1f)] float _fillAmount = 1f;

    private Image _fill;

    private void OnEnable() 
    {
        _fill = transform.Find("Mask").Find("Fill").GetComponent<Image>();
        _fillColor = _fill.color;

        switch(_type)
        {
            case StatType.CLEAN:
                _stats.onDirtynessChange += UpdateFill;
                break;
            case StatType.HUNGER:
                _stats.onHungerChange += UpdateFill;
                break;
            case StatType.HAPPY:
                _stats.onHappinessChange += UpdateFill;
                break;
        }
    }

    private void OnDisable()
    {
        switch(_type)
        {
            case StatType.CLEAN:
                _stats.onDirtynessChange -= UpdateFill;
                break;
            case StatType.HUNGER:
                _stats.onHungerChange -= UpdateFill;
                break;
            case StatType.HAPPY:
                _stats.onHappinessChange -= UpdateFill;
                break;
        }    
    }

    private void UpdateFill(float amount, bool additive = false)
    {
        if (!additive)
        {
            _fillAmount += amount;
        }
        else
        {
            _fillAmount = amount;
        }
    }

    private void Update() 
    {
        _fill.fillAmount = _fillAmount;
#if UNITY_EDITOR
        _fill.color = _fillColor;
#endif
    }
}
