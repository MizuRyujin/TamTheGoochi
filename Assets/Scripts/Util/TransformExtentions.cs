﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExtentions
{
    public static bool TryFind(this Transform t, string name, out Transform outTransform)
    {
        outTransform = t.Find(name);

        if (outTransform) return true;

        return false;
    }
}
