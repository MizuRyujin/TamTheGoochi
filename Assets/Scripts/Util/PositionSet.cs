﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionSet : MonoBehaviour
{
    [SerializeField] private Vector3 _position;
    [SerializeField] private Transform _resetParent;
    [SerializeField] private Collider _resetCollider;
    public UnityEngine.Events.UnityEvent _onReset;
    private Vector3 _startPosition;
    private Quaternion _startRotation;
    private Rigidbody _rigidBody;
    
    private void Awake() 
    {
        _rigidBody = GetComponent<Rigidbody>();
        _rigidBody.isKinematic = true;
        _rigidBody.useGravity = false;
    }

    private void OnEnable() {
        ResetPosition();
    }

    public void SetPosition()
    {
        transform.localPosition = _position;
        transform.rotation = Quaternion.identity;
    }

    private void Start() 
    {
        _startPosition = transform.localPosition;    
        _startRotation = transform.localRotation;

    }

    public void ResetPosition()
    {
        transform.localPosition = _startPosition;
        transform.localRotation = _startRotation;
        if (_rigidBody)
        {
            _rigidBody.velocity = Vector3.zero;
            _rigidBody.angularVelocity = Vector3.zero;
        } 

    }

    public void ResetTransform()
    {
        transform.parent = null;
    }

    public void PhysicsReset()
    {
        ResetPosition();
        transform.parent = _resetParent;
        if (_rigidBody)
        {
            _rigidBody.isKinematic = true;
            _rigidBody.useGravity = false;
        }
    
        _onReset?.Invoke();
    }

    public void ColliderStatus(bool value)
    {
        _resetCollider.enabled = value;
    }
}
