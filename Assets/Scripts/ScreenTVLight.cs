﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenTVLight : MonoBehaviour
{
    [SerializeField] private MeshRenderer _object = default;
    [SerializeField] private Color _turnOffColor = Color.red;
    [SerializeField] private Color _turnOnColor = Color.green;
    
    private Material _material;
    private bool _active = false;

    private void Awake() 
    {
        _material = _object.material;
           
    }

    public void SetLight(bool value)
    {
        _active = !_active;
        Color color = value ? _turnOnColor : _turnOffColor;
        ChangeMaterialColor(color);
    }

    public void SetLight()
    {
        _active = !_active;
        Color color = _active ? _turnOnColor : _turnOffColor;
        ChangeMaterialColor(color);
    }

    private void ChangeMaterialColor(Color c)
    {
        _material.SetColor("_Color", c);
        _material.SetColor("_Emission", c);
    }
}
