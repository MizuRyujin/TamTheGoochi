﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeckoController : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private Transform _mouthTransform;
    [SerializeField] private Transform _ballDropPoint;
    [SerializeField] private Transform _cleaningPoint;
    [SerializeField] private Transform _defaultPoint;

    // How fast we can turn and move full throttle
    [SerializeField] float _turnSpeed;
    [SerializeField] float _moveSpeed;
    // How fast we will reach the above speeds
    [SerializeField] float _turnAcceleration;
    [SerializeField] float _moveAcceleration;
    // Try to stay in this range from the target
    [SerializeField] float _minDistToTarget;
    [SerializeField] float _maxDistToTarget;
    // If we are above this angle from the target, start turning
    [SerializeField] float _maxAngToTarget;
    [SerializeField] float _maxPhysicsCheck;
    private Collider[] _cols;
    private Collider _lastCollider;
    private StatModifier _modifier;


    // World space velocity
    private Vector3 _currentVelocity;
    // We are only doing a rotation around the up axis, so we only use a float here
    private float _currentAngularVelocity;

    private void Awake()
    {
        _lastCollider = null;
    }

    private void Update()
    {
        RootMotionUpdate();
        CheckForStatModifier();
        DropBall();

        if (!_target.gameObject.activeInHierarchy && _target != _defaultPoint)
        {
            _target = _defaultPoint;
        }
    }

    private void RootMotionUpdate()
    {
        #region rotation
        // Get the direction toward our target
        Vector3 towardTarget = _target.position - transform.position;

        // Vector toward target on the local XZ plane
        Vector3 towardTargetProjected = Vector3.ProjectOnPlane(towardTarget, transform.up);

        // Get the angle from the gecko's forward direction to the direction toward our target
        // Here we get the signed angle around the up vector so we know which direction to turn in
        float angToTarget = Vector3.SignedAngle(transform.forward, towardTargetProjected, transform.up);

        float targetAngularVelocity = 0;

        // If we are within the max angle (i.e. approximately facing the target)
        // leave the target angular velocity at zero
        if (Mathf.Abs(angToTarget) > _maxAngToTarget)
        {
            // Angles in Unity are clockwise, so a positive angle here means to our right
            if (angToTarget > 0)
            {
                targetAngularVelocity = _turnSpeed;
            }
            // Invert angular speed if target is to our left
            else
            {
                targetAngularVelocity = -_turnSpeed;
            }
        }

        // Use smoothing function to gradually change the velocity
        _currentAngularVelocity = Mathf.Lerp(
          _currentAngularVelocity,
          targetAngularVelocity,
          1 - Mathf.Exp(-_turnAcceleration * Time.deltaTime)
        );

        // Rotate the transform around the Y axis in world space, 
        // making sure to multiply by delta time to get a consistent angular velocity
        transform.Rotate(0, Time.deltaTime * _currentAngularVelocity, 0, Space.World);
        #endregion
        #region translation
        Vector3 targetVelocity = Vector3.zero;

        // Don't move if we're facing away from the target, just rotate in place
        if (Mathf.Abs(angToTarget) < 90)
        {
            float distToTarget = Vector3.Distance(transform.position, _target.position);

            // If we're too far away, approach the target
            if (distToTarget > _maxDistToTarget)
            {
                targetVelocity = _moveSpeed * towardTargetProjected.normalized;
            }
            // If we're too close, reverse the direction and move away
            else if (distToTarget < _minDistToTarget)
            {
                targetVelocity = _moveSpeed * -towardTargetProjected.normalized;
            }
        }

        _currentVelocity = Vector3.Lerp(
          _currentVelocity,
          targetVelocity,
          1 - Mathf.Exp(-_moveAcceleration * Time.deltaTime)
        );

        // Apply the velocity
        transform.position += _currentVelocity * Time.deltaTime;
        #endregion
    }

    private void CheckForStatModifier()
    {
        _cols = Physics.OverlapSphere(
           transform.position, _maxPhysicsCheck, LayerMask.GetMask("Grabbable"));

        if (_cols.Length > 0)
        {
            if (_cols[0].TryGetComponent<StatModifier>(out StatModifier modifier))
            {
                if (modifier.ModifyingStat == StatType.HAPPY || 
                    modifier.ModifyingStat == StatType.HUNGER)
                {
                    _target = modifier.transform;
                }
                
                _lastCollider = _cols[0];
                _modifier = modifier;
            }
        }
        UsingStatModifier(_modifier);
    }

    private void UsingStatModifier(StatModifier modifier)
    {
        if (!modifier) return;
        float distToGrab = _maxDistToTarget / 2;
        if (Vector3.Distance(
            transform.position, modifier.transform.position) < distToGrab)
        {
            print("in range");
            switch (modifier.ModifyingStat)
            {
                case StatType.HAPPY:
                    if (modifier.TryGetComponent<Ball>(out Ball ball))
                    {
                        print("range of ball");
                        Animal animal = GetComponent<Animal>();
                        ball.PickUp(animal, _mouthTransform);
                        _target = ball.transform;
                    }
                    break;

                case StatType.CLEAN:
                    if (modifier.TryGetComponent<Shower>(out Shower shower))
                    {
                        print("CLEANING TIME");
                        Animal animal = GetComponent<Animal>();
                        _target = _cleaningPoint;
                    }
                    break;

                case StatType.HUNGER:
                    if (modifier.TryGetComponent<Food>(out Food food))
                    {
                        print("range of food");
                        Animal animal = GetComponent<Animal>();
                        food.Eat(animal);
                        _target = animal.transform;
                    }
                    break;
            }
        }
    }

    private void DropBall()
    {
        Ball ball = _mouthTransform.GetComponentInChildren<Ball>();
        if (ball)
        {
            print("Has ball");
            _target = _ballDropPoint;
            float distToDrop = _maxDistToTarget / 2;
            if (Vector3.Distance(
                transform.position, _ballDropPoint.transform.position) < distToDrop)
            {
                ball.Drop();
                ball.transform.position = _ballDropPoint.position;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, _minDistToTarget);
        Gizmos.DrawWireSphere(transform.position, _maxDistToTarget);
        Gizmos.DrawWireSphere(transform.position, _maxPhysicsCheck);

    }
}
